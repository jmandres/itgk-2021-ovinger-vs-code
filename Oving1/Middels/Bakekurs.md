### **[Intro øving 1](../Intro_Øving1.md)**
<br>


# Bakekurs


**Læringsmål:**

* Printe ut tekst til skjerm på en fin måte

* Kalkulasjoner i Python

**Starting Out with Python:**

* Kap. 2.3

* Kap. 2.8

* Kap. 8

## INTRODUKSJON

**Escape Character**

`\n` er nyttig å bruke her for å få en ny linje! Eks. `print(“hei\npå\ndeg”)` gir

*Utskrift*:
  
```
hei  
på  
deg
```

<br>

**ljust() og rjust()**

`streng.ljust(width)` returnerer strengen "left justified" i en streng av lengde `width`. `streng.rjust(width)` gjør det sammen bare at strengen blir "right justified". For eksempel blir `print('hei'.rjust(15))` til:

  
```python
           hei      #teksten printes altså ut etter 12 white spaces, ettersom strengen har lengde 3
```
    
Du kan lese mer om `rjust()` og `ljust()` [her](https://docs.python.org/2/library/stdtypes.html?highlight=rjust#str.rjust). 

<BR>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Bakekurs.py](Bakekurs.py)* !

**Introduksjon**

I denne oppgaven skal du beregne mengden ingredienser som trengs for å lage cookies og printe resultatet på et fint format. 

Du skal lage cookies og har en oppskrift som gir 48 cookies. Den oppskriften krever følgende ingredienser:

* 400 g sukker 
* 320 g smør
* 500 g sjokolade
* 2 egg 
* 460 g hvetemel 

## a)
Spør brukeren om hvor mange cookies han eller hun ønsker å bake og skriv ut hvor mange ingredienser som trengs for å lage så mange cookies.

Eksempel på kjøring:

  
```python
Hvor mange cookies ønsker du å bake? 24
Antall cookies: 24 
sukker(g): 200.0
smør(g): 160.0
sjokolade(g): 250.0
egg: 1.0
hvetemel(g): 230.0
```



## b)
Be brukeren om hvor mange cookies han eller hun ønsker å lage tre ganger og skriv ut ingrediensene på en fin og elegant måte. Du trenger kun å skrive ut antall cookies, og hvor mye sjokolade og sukker som trengs i gram.

Eksempel på kjøring:

  
```python
Hvor mange cookies vil du lage? 24
og hvor mange cookies vil du lage nå? 48
og hvor mange cookies vil du lage til slutt? 72
Antall cookies:        sukker(g)    sjokolade(g)
24                         200.0           250.0
48                         400.0           500.0
72                         600.0           750.0
```

`rjust()` og `ljust()` er nyttige funksjoner her, men for å bruke dem må man først konvertere antall cookies til en streng vha. `str()`. Andre ting som kan være nyttig er `\t` som lager et innrykk i teksten.



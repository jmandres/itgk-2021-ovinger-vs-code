# ITGK 2021 Øvinger VS-Code

Dette repoet brukes for å distribuere VS Code øvinger i ITGK høsten 2021.

Om du ikke ser noen andre filer, så kommer de etter hvert. I VS Code kan du gå inn i *Source Control*-fanen på venstre side, trykke på de tre prikkene i den menyen, og velge *Pull* for å sjekke etter nye filer.

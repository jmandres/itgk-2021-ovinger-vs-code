### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Doble løkker - til dels vanskelig

**Læringsmål:**

* Nøstede løkker

**Starting Out with Python:**

* Kap. 4.7

## OPPGAVER

Alle deloppgaver skal besvares her: *[Doble løkker.py](Doble%20løkker.py)* !

## a)
Skriv et program som benytter seg av en dobbel for-løkke og skriver ut følgende:


>1 <br>
1 2 <br>
1 2 3 <br>
1 2 3 4 <br>
1 2 3 4 5 <br>

<br>

## b)
Skriv et program som benytter seg av en dobbel for-løkke og skriver ut følgende:

```
X X
X  X
X   X
X    X
X     X
```

<br>

## c)

Skriv et program som lar brukeren gi som input fra tastaturet et positivt heltall. Programmet skal da skrive ut primtallsfaktoriseringen til tallet, eller evt. at det allerede er et primtall. Eksempel på et par kjøringer:

>*Skriv inn et positivt heltall*: 2 <br>
2 er et primtall

>*Skriv inn et positivt heltall:* 38 <br>
38 = 2*19

>*Skriv inn et positivt heltall:* 1000 <br>
1000 = 2*2*2*5*5*5

>*Skriv inn et positivt heltall:* 73727 <br>
73727 er et primtall

>*Skriv inn et positivt heltall:* 123456789 <br>
123456789 = 3*3*3607*3803


Dette er et problem som peker i retning av dobbel løkke fordi samme tall kan være faktor flere ganger, som f.eks. i `1000  = 2*2*2*5*5*5`. Den ytre løkka trengs for å prøve ut stadig nye faktorer, den indre for å prøve om igjen samme faktor, i tilfelle den inngår flere ganger i tallet.

<br>

## d)
Du skal hjelpe frøken Bernsen med å lage et enkelt program hvor elevene kan øve seg på den lille gangetabellen. Eleven skal stadig møte på nye gangestykker, og får 3 forsøk på hvert spørsmål. Benytt deg av randint(0,9) for å få tilfeldige tall for hvert gangestykke. Programmet skal fortsette frem til eleven gir beskjed om noe annet.

**Eksempel på kjøring:**


>*Hva blir 2 * 0?* 0 <br>
*Gratulerer, det er helt riktig! <br>
Er det ønskelig med flere spørsmål? Skriv 1 for ja og 0 for nei:* 1 <br><br>
*Hva blir 8 * 6?* 42 <br>
*Dessverre ikke riktig. Du har 2 forsøk igjen. <br>
Hva blir 8 * 6?* 48 <br>
*Gratulerer, det er helt riktig! <br>
Er det ønskelig med flere spørsmål? Skriv 1 for ja og 0 for nei:* 1 <br><br>
*Hva blir 8 * 9?* 73 <br>
*Dessverre ikke riktig. Du har 2 forsøk igjen. <br>
Hva blir 8 * 9?* 74 <br>
*Dessverre ikke riktig. Du har 1 forsøk igjen. <br>
Hva blir 8 * 9?* 78 <br>
*Dessverre ikke riktig. Du har 0 forsøk igjen. <br>
Dessverre klarte du ikke dette regnestykket, men vent så får du et nytt et:) <br>
Er det ønskelig med flere spørsmål? Skriv 1 for ja og 0 for nei:* 1 <br> <br>
*Hva blir 9 * 1?* 9 <br>
*Gratulerer, det er helt riktig! <br>
Er det ønskelig med flere spørsmål? Skriv 1 for ja og 0 for nei:* 0 <br>

<br>

## e) frivillig

Du skal endre programmet ditt fra d) slik at eleven først får veldig lette gangestykker ved å bruke randint(0,5) (fra random-biblioteket). Dersom eleven klarer å svare rett på 5 spørsmål på rad (eleven kan fremdeles bruke 3 forsøk), skal vanskelighetsgraden øke ved å bruke randint(0,10). Dette intervallet skal altså øke med 5 for hver gang eleven svarer korrekt på 5 spørsmål på rad. 



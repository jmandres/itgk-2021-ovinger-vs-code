### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Geometri

**Læringsmål:**

* Forstå at kode må være feilfri for å kjøre, og rette enkle syntaksfeil

* Debugging

**Starting Out with Python:**

* Kap. 2

<BR>

## INTRODUKSJON

### **Formatering av antall desimaler**
At omkretsen får en liten hale med ...002 skyldes at flyttall ikke kan lagres helt nøyaktig i dataens minne. Derfor kan det være ønskelig med kun to desimaler. Det er en innebygd funksjon i Python som tar seg av formatering og den ser slik ut: `format(tall, formatering)`. `tall` er her tallet som skal konverteres, mens formatering går ut på hvordan man ønsker å formere tallet. Et eksempel er format`(2/3, ‘.3f’)` som gir `0.667`. 3-tallet indikerer at det ønskes 3 desimaler.

<BR>

## OPPGAVER
Alle deloppgaver skal løses her: [Geometri.py](Geometri.py)!

I denne oppgaven skal vi rette feil i koden og endre utskriftsformat.

Norsklæreren din kunne utmerket godt lest en stil og forstått den på tross av noen småfeil, og til og med gitt god karakter hvis innholdet forøvrig var bra. Datamaskinen er ikke like tilgivende: I et Python-program må alt være pinlig nøyaktig, en enkelt skrivefeil kan være nok til at programmet slett ikke kjører.

## a) 
Rett alle syntaksfeilene i den utleverte koden slik at koden kjører uten problemer. Hver av syntaksfeilene her kan rettes ved å fjerne, legge til eller endre ett enkelt tegn, dvs. det er ikke nødvendig å gjøre større endringer i koden. Ved å kjøre programmet og se på feilmeldinger vil du få hint om hvor hver enkelt syntaksfeil befinner seg. Gjerne bruk VScode sin debugger. 

Når koden fungerer bør du få følgende output:
 
```python
Vi har en sirkel med radius 5
Omkretsen er 31.400000000000002
Arealet er 78.5
Sylinder med høyde 8 : Volumet er 628.0
```
#### **Hint**
Tenk på parenteser, fnutter, variabelnavn og (+/,) i kombinasjon av tekst og variabelnavn.

## b)
Endre koden fra **a)** til å skrive ut omkretsen med kun 2 desimaler. Dvs. at linje to over skal bli endret til Omkretsen er 31.40.

Kjøring av oppdatert kode:

```python
Vi har en sirkel med radius 5
Omkretsen er 31.40
Arealet er 78.5
Sylinder med høyde 8 : Volumet er 628.0
```

## c)
(frivillig vanskelig oppgave) I oppgave a får omkretsen verdien 31.400...002, hvorfor blir ikke omkretsen sin verdi lik 31.400...000?
Var som en kommentar i koden.